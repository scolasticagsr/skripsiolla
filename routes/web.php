<?php

use Illuminate\Support\Facades\Route;
use App\Edom;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('home');
});
// Route::get('/', 'AuthController@nist');
Route::get('/lupapassword', function () {
	return view('lupaPassword');
});

//AuthController
Route::get('/index', 'AuthController@index');
Route::post('/postLogin', 'AuthController@login');
Route::post('/verifotp/{_token}', 'AuthController@verifikasiotp');
Route::get('/logout', 'AuthController@logout');
Route::get('/resend/login', 'AuthController@resend_login');

//ResetPassController
Route::get('/indexreset', 'ResetPasswordController@index');
Route::post('/postReset', 'ResetPasswordController@postReset');
Route::post('/verifemail', 'ResetPasswordController@verifikasiemail');
Route::post('/resetpass','ResetPasswordController@resetpass');
Route::get('/resend/reset', 'ResetPasswordController@resend_reset');

Route::post('/importJadwal', "JadwalController@importJadwal");
Route::post('/importMahasiswa', "MahasiswaController@importMahasiswa");
Route::get('/pdf', 'EdomController@export_pdf');

Route::resource('mahasiswa', 'MahasiswaController');
Route::resource('jurusan', 'JurusanController');
Route::resource('matakuliah', 'MataKuliahController');
Route::resource('dosen', 'DosenController');
Route::resource('jadwal', 'JadwalController');
Route::resource('edom', 'EdomController');
Route::resource('kelas', 'KelasController');
Route::resource('formedom', 'FormEdomController');
Route::resource('verifikasiemail', 'OTPreset');
Route::resource('gantipassword', 'gantipassword');
Route::resource('login', 'Login');
Route::resource('verifikasiotp', 'OTPlogin');
Route::resource('hasil', 'HasilController');

