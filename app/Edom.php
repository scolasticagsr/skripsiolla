<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edom extends Model
{
    protected $table='bukti_edom';
    protected $fillable = [
    	'mata_kuliah', 'periode', 'semester', 'nama_dosen', 'status'
    ];
}
