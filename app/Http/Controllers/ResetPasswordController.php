<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\ModelUser;
use App\Mahasiswa;
use App\ModelOtpReset;
use DB;
use Carbon\Carbon;
class ResetPasswordController extends Controller
{
    public function index()
    {
        if(!Session::get('reset'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            return redirect()->route('verifikasiemail.index');
        }
    }

    public function postReset(Request $request)
    {
        $email = $request->email;
        $data = Mahasiswa::where('email',$email)->first();
        $data2 = ModelOtpReset::where('email',$email)->first();

        $this->validate($request, [
            'email' => 'required|email'
        ]);

        if($data) {
            Session::put('nama_mahasiswa',$data->nama_mahasiswa);
            Session::put('nim',$data->nim);
            Session::put('email',$data->email);
            Session::put('password',$data->password);
            Session::put('kode_jurusan',$data->kode_jurusan);
            Session::put('kode_kelas',$data->kode_kelas);
            Session::put('reset',TRUE);

            $now = now();
            $salt = "resetpasswordedompnj";
            $reset = $email.$salt.$now;
            $sha1 = sha1($reset);
            $numerik = preg_replace('/[^1-9]/','', $sha1);
            $otp_reset = substr(str_shuffle($numerik), 0, 6);
            Session::put('email',$request->email);
            Session::put('kode_otp_reset',$otp_reset);

            Mail::send('emailOtpReset', ['nama' => $request->nama, 'email' => $request->email], function ($message) use ($data) {
                $message->from('donotreply@edom.com', 'EDOM PNJ');
                $message->to($data->email)->subject('Lupa Password');
            });
            
            if($data2) {
                ModelOtpReset::where('email', $email)->update(['otp_reset' => $otp_reset]);
            }
            else {
                $otp = new ModelOtpReset();
                $otp->email = $request->email;
                $otp->otp_reset = $otp_reset;
                $otp->save();
            }
            return redirect('/indexreset'); 
        }
        return redirect('/lupapassword')->with('alert','Email tidak terdaftar!');
    }

    public function verifikasiemail(Request $request)
    {
        $email = $request->session()->get('email');
        $updated_at = DB::table('otp_reset')->select('updated_at')->where('email', $email)->first();
        
        if(!Session::get('reset')) {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else {
            $enteredotp = $request->kode_otp;
            $data = ModelOtpReset::where('email',$email)->first();
            $OTP = $request->session()->get('kode_otp_reset');

            if($data) {
                if($OTP === $enteredotp) {
                    if(Carbon::now()->diffInSeconds($updated_at->updated_at) < 60) {
                        Session::forget('kode_otp_reset');
                        return redirect()->route('gantipassword.index');
                    }
                    return redirect()->route('verifikasiemail.index')->with('alert','Kode OTP expired!');
                }
                return redirect()->route('verifikasiemail.index')->with('alert','Kode OTP tidak sesuai!');    
            }
            return redirect()->route('verifikasiemail.index')->with('alert','Kode OTP tidak sesuai!');  
        }
    }

    public function resetpass(Request $request)
    {
        if(!Session::get('reset'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
        $email = $request->session()->get('email');
        $pass1 = $request->pass1;
        $pass2 = $request->pass2;

        if($pass1 == $pass2) {
            if(strlen($pass1)<=6 || strlen($pass2)<=6) {
                return redirect()->route('gantipassword.index')->with('alert', 'Password minimal 6 karakter!');
            }  
            Mahasiswa::where('email', $email)->update(['password' => bcrypt($pass2)]);
            Session::forget('email');
            Session::put('login', TRUE);
            return redirect()->route('edom.index');
        }
        return redirect()->route('gantipassword.index')->with('alert','Konfirmasi password baru tidak sesuai!');
        }
    }

    public function resend_reset()
    {
        $email = Session::get('email');
        $data = ModelOtpReset::where('email',$email)->first();
        $now = now();
        $sha1 = sha1($email.$now."resetpasswordedompnj");
        $numerik = preg_replace('/[^0-9]/','', $sha1);
        $otp_reset = substr(str_shuffle($numerik), 0, 6);
        ModelOtpReset::where('email', $email)->update(['otp_reset' => $otp_reset]);
        Session::put('email',$email);
        Session::put('kode_otp_reset',$otp_reset);

        Mail::send('emailOtpReset', ['email' => $email], function ($message) use ($data) {
            $message->from('donotreply@edom.com', 'EDOM PNJ');
            $message->to($data->email)->subject('Lupa Password');
        }); 
        return redirect()->route('verifikasiemail.index')->with('alert-success', 'Kode sudah dikirim ulang');
    }
}
