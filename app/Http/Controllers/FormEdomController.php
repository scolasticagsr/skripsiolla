<?php

namespace App\Http\Controllers;

use App\FormEdom;
use App\Jadwal;
use App\HasilEdom;
use\Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use DB;

class FormEdomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        if(!Session::get('login'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            return redirect('/login');
        }
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // return view('formEdom');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // $data = new Edom();
        // $data->nilai= $request->nilai;
        // $data->status = "100%";
        // $data->save();
        // return redirect()->route('edom.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormEdom  $formEdom
     * @return \Illuminate\Http\Response
     */
    public function show(FormEdom $formEdom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormEdom  $formEdom
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data2 = Jadwal::where('id', $id)->get();
        $data = FormEdom::all();
        $nip = DB::table('jadwal')->select('nip')->where('id', $id)->first();
        $kode_matakuliah = DB::table('jadwal')->select('kode_matakuliah')->where('id', $id)->first();
        $periode = DB::table('jadwal')->select('periode')->where('id', $id)->first();
        $semester = DB::table('jadwal')->select('semester')->where('id', $id)->first();
        $nama_dosen = DB::table('dosen')->select('nama_dosen')->where('nip', $nip->nip)->first();
        $mata_kuliah = DB::table('mata_kuliah')->select('mata_kuliah')->where('kode_matakuliah', $kode_matakuliah->kode_matakuliah)->first();
        Session::put('nip', $nip->nip);
        Session::put('kode_matakuliah', $kode_matakuliah->kode_matakuliah);
        Session::put('nama_dosen', $nama_dosen->nama_dosen);
        Session::put('mata_kuliah', $mata_kuliah->mata_kuliah);
        Session::put('periode', $periode->periode);
        Session::put('semester', $semester->semester);
        return view('formEdom', compact('data', 'data2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormEdom  $formEdom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasil = 0;
        for($i=1; $i<28; $i++) {
            $idd = DB::table('pertanyaan')->select('id')->where('id', '=', $i)->first();
            $pertanyaan = $idd->id;
            $nilai = $request->nilai[$pertanyaan];
            $hasil += $nilai;
            $final = ($hasil/162)*100;
        }

        $data = Jadwal::where('id', $id)->first();
        $data->status = "100%";
        $data->save();
        $nip = Session::get('nip');
        $kode_matakuliah = Session::get('kode_matakuliah');
        $data2 = HasilEdom::where('nip', $nip)->where('kode_matakuliah', $kode_matakuliah)->first();
        $hasil = DB::table('hasiledom')->select('hasil')->where('nip', $nip)->where('kode_matakuliah', $kode_matakuliah)->first();
        $hasil2 = $hasil->hasil;
        $responden = DB::table('hasiledom')->select('responden')->where('nip', $nip)->where('kode_matakuliah', $kode_matakuliah)->first();
        $resp = $responden->responden;
        $result = ($hasil2 + $final)/$resp += 1;
        $data2->hasil = number_format($result,2);
        $data2->responden = $resp;
        $data2->save();
        return redirect()->route('edom.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormEdom  $formEdom
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormEdom $formEdom)
    {
        //
    }
}
