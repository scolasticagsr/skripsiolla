<?php

namespace App\Http\Controllers;

use App\MataKuliah;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('admin'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            $data = MataKuliah::all();
            return view('adminMataKuliah', compact('data'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahMataKuliah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_matakuliah' => 'required',
            'mata_kuliah' => 'required']);

        $data = new MataKuliah();
        $data->kode_matakuliah = $request->kode_matakuliah;
        $data->mata_kuliah = $request->mata_kuliah;
        $data->save();

        Session::forget('kode_matakuliah');
        Session::forget('mata_kuliah');

        return redirect()->route('matakuliah.index')->with('alert-success', 'Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MataKuliah  $mataKuliah
     * @return \Illuminate\Http\Response
     */
    public function show(MataKuliah $mataKuliah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MataKuliah  $mataKuliah
     * @return \Illuminate\Http\Response
     */
    public function edit(MataKuliah $mataKuliah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MataKuliah  $mataKuliah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MataKuliah $mataKuliah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MataKuliah  $mataKuliah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MataKuliah::where('id',$id)->first();
        $data->delete();
        return redirect()->route('matakuliah.index')->with('alert-success','Data berhasil dihapus!');
    }
}
