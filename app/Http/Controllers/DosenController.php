<?php

namespace App\Http\Controllers;

use App\Dosen;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('admin'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            $data = Dosen::all();
            return view('adminDosen', compact('data'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahDosen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required',
            'nama_dosen' => 'required|min:5']);

        $data = new Dosen();
        $data->nip = $request->nip;
        $data->nama_dosen = $request->nama_dosen;
        $data->save();

        Session::forget('nip');
        Session::forget('nama_dosen');

        return redirect()->route('dosen.index')->with('alert-success', 'Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function show(Dosen $dosen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function edit(Dosen $dosen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dosen $dosen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function destroy($nip)
    {
        $data = Dosen::where('nip',$nip)->first();
        $data->delete();
        return redirect()->route('dosen.index')->with('alert-success','Data berhasil dihapus!');
    }
}
