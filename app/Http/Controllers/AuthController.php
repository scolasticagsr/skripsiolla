<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\ModelUser;
use App\ModelOtp;
use App\Mahasiswa;
use Timestamp;
use DB;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function index()
    {
        if(!Session::get('login'))
        {
            return redirect('/login');
        }
        else
        {
            return redirect()->route('verifikasiotp.index');
        }
    }

    public function nist()
    {
        $email = "scolastica.graciashintarivamella.tik16@mhsw.pnj.ac.id";
        $salt = "resetpasswordedompnj";
        $now = now();
        $var = $email.$salt.$now;
        for($i=1; $i<7501; $i++)
        {
            $var2 = $var.$i;
            $sha = sha1($var2);
            $numerik = preg_replace('/[^1-9]/','', $sha);
            $otp_login = substr(str_shuffle($numerik), 0, 6);
            echo "$sha";
        }
    }

    public function login(Request $request)
    {
        $nim = $request->nim;
        $password = $request->password;
        $data = Mahasiswa::where('nim',$nim)->first();
        $data2 = ModelUser::where('nim',$nim)->first();

        if($data)
        {
            if(Hash::check($password,$data->password))
            {
                Session::put('nama_mahasiswa',$data->nama_mahasiswa);
                Session::put('nim',$data->nim);
                Session::put('email',$data->email);
                Session::put('password',$data->password);
                Session::put('kode_jurusan',$data->kode_jurusan);
                Session::put('kode_kelas',$data->kode_kelas);
                Session::put('login',TRUE);
                
                $now = now();
                $login = $nim.$password.$now;
                $sha1 = sha1($login);
                $numerik = preg_replace('/[^1-9]/','', $sha1);
                $otp_login = substr(str_shuffle($numerik), 0, 6);
                Session::put('otp_login',$otp_login);                
                
                Mail::send('emailOtpLogin', ['nama_mahasiswa' => $request->nama_mahasiswa, 'nim' => $request->nim], function ($message) use ($data)
                {
                    $message->from('donotreply@edom.com', 'EDOM PNJ');
                    $message->to($data->email)->subject('Kode OTP');
                });

                Mahasiswa::where('nim', $nim)->update(['otp_login' => $otp_login]);
                return redirect('/index');
            }
            return redirect('/login')->with('alert','NIM atau password salah!');
        }
        else if($data2)
        {
            if(Hash::check($password,$data2->password))
            {
                Session::put('user',$data2->nim);
                Session::put('nama',$data2->nama);
                Session::put('password',$data2->password);
                Session::put('admin',TRUE);
                return redirect()->route('mahasiswa.index');
            }
            return redirect('/login')->with('alert','NIM atau password salah!');
        }
        return redirect('/login')->with('alert','NIM atau password salah!');
    }

    public function verifikasiotp(Request $request)
    {
        $nim = $request->session()->get('nim');
        $updated_at = DB::table('mahasiswa')->select('updated_at')->where('nim', $nim)->first();
        if(!Session::get('login')) {
            return redirect('/login');
        }
        else {
            $enteredotp = $request->kode_otp;
            $OTP = $request->session()->get('otp_login');
            $data = Mahasiswa::where('nim',$nim)->first();

            if($data) {
                if($OTP === $enteredotp) {
                    if(Carbon::now()->diffInSeconds($updated_at->updated_at) < 60) {
                    Session::forget('otp_login');
                    return redirect()->route('edom.index');
                    }
                    return redirect()->route('verifikasiotp.index')->with('alert', 'Kode OTP expired!');
                }
                return redirect()->route('verifikasiotp.index')->with('alert','Kode OTP tidak sesuai!');
            }
            return redirect()->route('verifikasiotp.index')->with('alert','Kode OTP tidak sesuai!');
        }
    }

    public function resend_login()
    {
        $nim = Session::get('nim');
        $password = Session::get('password');
        $nama_mahasiswa = Session::get('nama_mahasiswa');
        $data = Mahasiswa::where('nim',$nim)->first();
        
        $now = now();
        $sha1 = sha1($nim.$password.$now);
        $numerik = preg_replace('/[^0-9]/','', $sha1);
        $otp_login = substr(str_shuffle($numerik), 0, 6);
        Session::put('otp_login',$otp_login);

        Mail::send('emailOtpLogin', ['nama_mahasiswa' => $nim, 'nim' => $nama_mahasiswa], function ($message) use ($data) {
            $message->from('donotreply@edom.compact(varname)', 'EDOM PNJ');
            $message->to($data->email)->subject('Kode OTP');
        });
        Mahasiswa::where('nim', $nim)->update(['otp_login' => $otp_login]);
        return redirect()->route('verifikasiotp.index')->with('alert-success', 'Kode sudah dikirim ulang');
    }

    public function logout()
    {
        Session::flush();
        Session::forget('nim');
        return redirect('/login')->with('alert-success','Berhasil logout!');
    }
}
