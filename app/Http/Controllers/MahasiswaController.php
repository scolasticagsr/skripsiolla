<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mahasiswa;
use App\ModelOtpReset;
use Illuminate\Http\Request;
use Importer;
use DB;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(!Session::get('admin'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
        $data = Mahasiswa::all();
        return view('adminMahasiswa', compact('data'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahMahasiswa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nim' => 'required|unique:mahasiswa',
            'nama_mahasiswa' => 'required|min:4',
            'email' => 'required|min:4|email|unique:mahasiswa',
            'jurusan' => 'required',
            'prodi' => 'required',
            'nama_kelas' => 'required']);
            
        $str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
        $length = 6;
        $pass = substr(str_shuffle($str), 0, $length);

        $mhs = new Mahasiswa();
        $mhs->nim = $request->nim;
        $mhs->nama_mahasiswa = $request->nama_mahasiswa;
        $mhs->email = $request->email;
        $mhs->password = bcrypt($pass);
        $mhs->jurusan = $request->jurusan;
        $mhs->prodi = $request->prodi;
        $mhs->nama_kelas = $request->nama_kelas;
        $mhs->save();
        
        Session::put('nim',$mhs->nim);
        Session::put('nama_mahasiswa',$mhs->nama_mahasiswa);
        Session::put('password',$pass);
        $email = $request->email;
        $data = Mahasiswa::where('email',$email)->first();

        Mail::send('emailPassword', ['password' => $pass, 'email' => $request->email], function ($message) use ($data)
        {
            $message->from('donotreply@edom.com', 'EDOM PNJ');
            $message->to($data->email)->subject('Selamat Datang di Politeknik Negeri Jakarta');
        });

        return redirect()->route('mahasiswa.index')->with('alert-success', 'Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        $data = Mahasiswa::where('nim',$nim);
        $data->delete();
        return redirect()->route('mahasiswa.index')->with('alert-success','Data berhasil dihapus!');
    }

    public function importMahasiswa(Request $request)
    {
        if(!Session::get('admin')) {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls'
        ]);

        $excel = Importer::make('Excel');
        $excel->load($request->file('file'));
        $excel->setSheet(1);
        $collection = $excel->getCollection();
        $counter = count($collection);

        for($i=1; $i<sizeof($collection); $i++) {
            $collect = $collection[$i];
            $str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
            $length = 6;
            $pass = substr(str_shuffle($str), 0, $length);
            $insert_data[] = array(
                'nim' => $collect[0],
                'nama_mahasiswa' => $collect[1],
                'email' => $collect[2],
                'password' => $pass,
                'kode_jurusan' => $collect[3],
                'kode_kelas' => $collect[4] 
                );
        }

        if(!empty($insert_data)) {
            DB::table('mahasiswa')->insert($insert_data);
            $count = count($insert_data);

            for($x=0; $x<$count; $x++) {
                $nama_mahasiswa = $insert_data[$x]['nama_mahasiswa'];
                $nim = $insert_data[$x]['nim'];
                $email = $insert_data[$x]['email'];
                $password = $insert_data[$x]['password'];
                Session::put('nama_mahasiswa', $nama_mahasiswa);
                Session::put('nim', $nim);
                Session::put('password', $password);

                Mail::send('emailPassword', ['password' => $pass], function ($message) use ($email)
                {
                    $message->from('donotreply@edom.com', 'EDOM PNJ');
                    $message->to($email);
                    $message->subject('Selamat Datang di Politeknik Negeri Jakarta');
                });

                DB::table('mahasiswa')->where('email', $email)->update(array('password' => bcrypt($password)));
            }
            return redirect()->route('mahasiswa.index')->with('alert-success','Data mahasiswa berhasil diimport!');       
        }
        }          
    }
}
