<?php

namespace App\Http\Controllers;

use App\Jurusan;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use DB;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('admin'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            $data = Jurusan::all();
            return view('adminJurusan', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahJurusan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_jurusan' => 'required',
            'jurusan' => 'required|min:4',
            'prodi' => 'required']);

        $data = new Jurusan();
        $data->kode_jurusan = $request->kode_jurusan;
        $data->jurusan = $request->jurusan;
        $data->prodi = $request->prodi;
        $data->save();

        Session::forget('kode_jurusan');
        Session::forget('jurusan');
        Session::forget('prodi');
        return redirect()->route('jurusan.index')->with('alert-success', 'Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jurusan  $jurusan
     * @return \Illuminate\Http\Response
     */
    public function show(Jurusan $jurusan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurusan  $jurusan
     * @return \Illuminate\Http\Response
     */
    public function edit(Jurusan $jurusan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurusan  $jurusan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jurusan $jurusan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurusan  $jurusan
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_jurusan)
    {
        $data = Jurusan::where('kode_jurusan',$kode_jurusan);
        $data->delete();
        return redirect()->route('jurusan.index')->with('alert-success','Data berhasil dihapus!');
    }
}
