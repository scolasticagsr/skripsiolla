<?php

namespace App\Http\Controllers;

use App\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('admin'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            $data = Kelas::all();
            return view('adminKelas', compact('data'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahKelas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_kelas' => 'required',
            'nama_kelas' => 'required']);

        $data = new Kelas();
        $data->kode_kelas = $request->kode_kelas;
        $data->nama_kelas = $request->nama_kelas;
        $data->save();

        Session::forget('kode_kelas');
        Session::forget('nama_kelas');

        return redirect()->route('kelas.index')->with('alert-success', 'Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelas $kelas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_kelas)
    {
        $data = Kelas::where('kode_kelas',$kode_kelas);
        $data->delete();
        return redirect()->route('kelas.index')->with('alert-success','Data berhasil dihapus!');
    }
}
