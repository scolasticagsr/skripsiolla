<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
        public function postLogin(Request $request)
    {
        $nim = $request->nim;
        $password = $request->password;

        $data = Mahasiswa::where('nim',$nim)->first();
        $data2 = ModelUser::where('nim',$nim)->first();
        if($data)
        {
            if(Hash::check($password,$data->password))
            {
                Session::put('nama_mahasiswa',$data->nama_mahasiswa);
                Session::put('nim',$data->nim);
                Session::put('email',$data->email);
                Session::put('password',$data->password);
                Session::put('jurusan',$data->jurusan);
                Session::put('prodi',$data->prodi);
                Session::put('nama_kelas',$data->nama_kelas);
                Session::put('login',TRUE);
                
                //generate OTP
                $kode1 = sha1($nim.$password);
                $kode2 = preg_replace('/[^0-9]/','', $kode1);
                $length = 6;
                $kode_otp_login = substr(str_shuffle($kode2), 0, $length);

                //save OTP to database
                $otp = new ModelOtp();
                $otp->kode_otp_login = $kode_otp_login;
                $otp->nim = $request->nim;
                $otp->isVerified = "0";
                $otp->save();

                if($otp->save())
                {
                    $res['now'] = $now;
                    $res['sha1'] = $sha1;
                    $res['numerik'] = $numerik;
                    $res['otp login'] = $otp_login;
                    return response($res);
                }
                
                Session::put('kode_otp_login',$kode_otp_login);

                //send OTP to email
                Mail::send('emailOtpLogin', ['nama_mahasiswa' => $request->nama_mahasiswa, 'nim' => $request->nim], function ($message) use ($data)
                {
                    $message->from('donotreply@edom.com', 'EDOM PNJ');
                    $message->to($data->email)->subject('Kode OTP');
                });
                return redirect('/index'); 
            }
            return redirect('/login')->with('alert','NIM atau password salah!');
        }
        if($data2)
        {
            if(Hash::check($password,$data2->password))
            {
                Session::put('nim',$data2->nim);
                Session::put('nama',$data2->nama);
                Session::put('password',$data2->password);
                Session::put('login',TRUE);
                return redirect('/admin/home');
            }
            return redirect('/login')->with('alert','NIM atau password salah!');
        }
        return redirect('/')->with('alert','NIM atau password salah!');
    }

    public function verifikasiotp(Request $request)
    {
        $enteredotp = $request->kode_otp;
        $OTP = $request->session()->get('kode_otp_login');

        if($OTP === $enteredotp)
        {
            ModelOtp::where('kode_otp_login', $enteredotp)->update(['isVerified' => 1]);
            Session::forget('kode_otp_login');
            return redirect()->route('edom.index');
        }
        return redirect('/otplogin')->with('alert','Kode OTP tidak sesuai! ');
    }

     public function login(Request $request)
    {
        $nim = $request->nim;
        $password = $request->password;
        $data = Mahasiswa::where('nim',$nim)->first();

        if($data) {
            if(Hash::check($password,$data->password)) {
                Session::put('nama_mahasiswa',$data->nama_mahasiswa);
                Session::put('nim',$data->nim);
                Session::put('email',$data->email);
                Session::put('password',$data->password);
                Session::put('kode_jurusan',$data->kode_jurusan);
                Session::put('kode_kelas',$data->kode_kelas);
                Session::put('login',TRUE);
                
                $now = now();
                $sha1 = sha1($nim.$password.$now);
                $numerik = preg_replace('/[^0-9]/','', $sha1);
                $otp_login = substr(str_shuffle($numerik), 0, 6);
                Mahasiswa::where('nim', $nim)->update(['otp_login' => $otp_login]);
                Session::put('otp_login',$otp_login);

                Mail::send('emailOtpLogin', ['nama_mahasiswa' => $request->nama_mahasiswa, 'nim' => $request->nim], function ($message) use ($data)
                {
                    $message->from('donotreply@edom.compact(varname)', 'EDOM PNJ');
                    $message->to($data->email)->subject('Kode OTP');
                });
                return redirect('/index');
            }
            return redirect('/login')->with('alert','NIM atau password salah!');
        }
        return redirect('/login')->with('alert','NIM atau password salah!');
    }
}
