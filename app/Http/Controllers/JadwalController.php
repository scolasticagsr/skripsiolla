<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Imports\ImportJadwal;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Importer;
use DB;
use App\Edom;
use Timestamps;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Jadwal::all();
        return view('adminJadwal', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(Jadwal $jadwal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit(Jadwal $jadwal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jadwal $jadwal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Jadwal::where('id',$id)->first();
        $data->delete();
        return redirect()->route('jadwal.index')->with('alert-success','Data berhasil dihapus!');
    }

    public function importJadwal(Request $request)
    {
        if(!Session::get('admin')) {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else {
            $this->validate($request, [
                'file' => 'required|mimes:xlsx,xls'
            ]);

            $excel = Importer::make('Excel');
            $excel->load($request->file('file'));
            $excel->setSheet(1);
            $collection = $excel->getCollection();
            $counter = count($collection);

            for($i=1; $i<sizeof($collection); $i++)
            {
                $collect = $collection[$i];
                $insert_data[] = array(
                    'nim' => $collect[0],
                    'kode_matakuliah' => $collect[1],
                    'kode_jurusan' => $collect[2],
                    'kode_kelas' => $collect[3],
                    'periode' => $collect[4],
                    'semester' => $collect[5],
                    'nip' => $collect[6],
                    'created_at' => now()
                    );
            }

            if(!empty($insert_data))
            {
                DB::table('jadwal')->insert($insert_data);
                return redirect()->route('jadwal.index')->with('alert-success','Data jadwal berhasil diimport!');
            }
            else
            {
                return redirect()->route('jadwal.index')->with('alert','Data jadwal gagal diimport!');
            }
        }
    }
}
