<?php

namespace App\Http\Controllers;

use Auth;
use App\Jadwal;
use App\Jurusan;
use App\Mahasiswa;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use PDF;
use DB;

class EdomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('login'))
        {
            return redirect('/login')->with('alert','Anda harus login');
        }
        else
        {
            $value = Session::get('nim');
            $value2 = Session::get('kode_kelas');
            $value3 = Session::get('kode_jurusan');
            $data2 = DB::table('jurusan')->select('prodi', 'jurusan')->where('kode_jurusan', $value3)->first();
            $prodi = $data2->prodi;
            $jurusan = $data2->jurusan;
            Session::put('prodi', $prodi);
            Session::put('jurusan', $jurusan);
            $data = Jadwal::where('nim', '=', $value)->where('kode_kelas', '=', $value2)->get();
            return view('edom', compact('data'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Edom  $edom
     * @return \Illuminate\Http\Response
     */
    public function show(Edom $edom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Edom  $edom
     * @return \Illuminate\Http\Response
     */
    public function edit(Edom $edom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Edom  $edom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Edom $edom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Edom  $edom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Edom $edom)
    {
        //
    }

    public function export_pdf()
    {
        $value = Session::get('nim');
        $value2 = Session::get('kode_kelas');
        $value3 = Session::get('kode_jurusan');
        $data = Jadwal::where('nim', '=', $value)->where('kode_kelas', '=', $value2)->get();
        $datas = DB::table('jurusan')->select('prodi')->where('kode_jurusan', $value3)->first();
        $prodi = $datas->prodi;
        Session::put('prodi', $prodi);
        $pdf = PDF::loadView('edomPDF', compact('data'));
        return $pdf->download('BuktiPengisianEdom.pdf');
    }

    public function csrf_field()
    {
       return new HtmlString('<input type="hidden" name="_token" value="'.csrf_token().'">');
    }
}
