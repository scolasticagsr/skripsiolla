<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
  <title>Edom | OTP</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="home.css">
    <style type="text/css">
    .card{
height: 430px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}
  </style>
</head>
<body>
  <br>
<div class="container">
  <div class="d-flex h-100 justify-content-center">
    <div class="card">
      <div class="card-header">
        <h3 style="text-align: center; font-weight: bold;">Verifikasi Email</h3>
      </div>
      <div class="card-body">
        <form class="form-signin" method="POST" action="/verifemail">
          @if(\Session::has('alert'))
          <div class="alert alert-danger">
            <div>{{ Session::get('alert') }}</div>
          </div>
          @endif
          @if(\Session::has('alert-success'))
          <div class="alert alert-success">
            <div>{{ Session::get('alert-success') }}</div>
          </div>
          @endif
          {{csrf_field()}}
          <div class="text-center">
             <img class="mb-4 center" src="logo pnj.png" alt="" width="150">
             <p style="color: white; font-size: 13px; font-weight: bold; text-align: left;">Kode OTP dikirim ke email Anda</p>
          </div>
          <div class="input-group form-group">
            <label for="inputotp" class="control-label sr-only">OTP</label>
            <input name="kode_otp" type="kode_otp" id="inputotp" class="form-control" placeholder="Masukkan 6 digit" required autofocus>
          </div>
            <div class="form-group">
              <input type="submit" value="Submit" class="btn-sm float-right login_btn" style="font-size: 11px; text-align: center;">
              <a href="/resend/reset" style="color: white; font-size: 13px;">Kirim ulang kode?</a>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--   <script type="text/javascript">
    var timeleft = 60;
    var downloadTimer = setInterval(function(){
      if(timeleft <= 0){
        clearInterval(downloadTimer);
        alert('Kode OTP expired!');
      } else {
        document.getElementById("countdown").innerHTML = timeleft;
      }
      timeleft -= 1;
    }, 1000);
  </script> -->
</body>
</html>