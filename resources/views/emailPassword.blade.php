<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Boardicle Email</title>
    <style>
        .body {
            background-color: #f6f6f6;
            width: 100%; }

        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px; }

        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
            padding: 10px; }

        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%; }

        .wrapper {
            box-sizing: border-box;
            padding: 20px; }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize; }
    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">
                <table class="main">
                    <tr>
                        <td class="wrapper">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <p style="font-size: 20px; text-align: center;">Selamat datang, {{ Session::get('nama_mahasiswa') }}!</p>
                                        <br>
                                        <p>Berikut informasi login Anda untuk login ke EDOM PNJ
                                            <table class="table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th style="font-weight: bold; text-align: left;">NIM</th>
                                                        <td>: {{ Session::get('nim') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th style="font-weight: bold;">Password:</th>
                                                        <td>: {{ Session::get('password') }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
<!--                                         <p>NIM     : {{ Session::get('nim') }}
                                        <p>Password : {{ Session::get('password') }}</p> -->
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                            <tr>
                                                <td align="left">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td> <div class="container">
                                                                    <br>
                                                                    <br>
                                                                    <br>
                                                                    <center><p>Terima kasih!</p></center>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {{--<p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>--}}
                                        {{--<p>Good luck! Hope it works.</p>--}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>