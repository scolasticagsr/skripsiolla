
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">

    <title>EDOM PNJ</title>

    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="resetpass.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <div class="jumbotron">
      <form class="form-signin" method="POST" action="/resetpass">
        @if(\Session::has('alert'))
        <div class="alert alert-danger">
          <div>{{ Session::get('alert') }}</div>
        </div>
        @endif
        @if(\Session::has('alert-success'))
        <div class="alert alert-success">
          <div>{{ Session::get('alert-success') }}</div>
        </div>
        @endif
        
        {{ csrf_field() }}
        <img class="mb-4" src="logo pnj.png" alt="" width="250">
        <h1 class="h3 mb-3 font-weight-normal">Hallo, {{Session::get('nama')}}</h1>
        <div class="form-group">
          <label for="inputpass1" class="control-label sr-only">Pass1</label>
          <input name="pass1" type="password" id="inputpass1" class="form-control" placeholder="Masukkan password baru" required autofocus>
        </div>
        <div class="form-group">
          <label for="inputpass2" class="control-label sr-only">Pass2</label>
          <input name="pass2" type="password" id="inputpass2" class="form-control" placeholder="Konfirmasi password baru" required>
        </div>
        <div class="form-group clearfix">
          
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block" >Reset</button>
        <br>
      </form>
    </div>
</body>
</html>
