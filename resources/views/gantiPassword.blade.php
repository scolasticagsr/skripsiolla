 <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
  <title>Edom | Reset</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="home.css">

    <style type="text/css">
    .card{
height: 430px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}
  </style>
</head>
<body>
  <br>
<div class="container">
  <div class="d-flex justify-content-center h-100">
    <div class="card">
      <div class="card-header">
        <h3 style="text-align: center; font-weight: bold;">Ganti password</h3>
      </div>
      <div class="card-body">
        <form class="form-signin" method="POST" action="/resetpass">
          @if(\Session::has('alert'))
          <div class="alert alert-danger">
            <div style="font-size: 12px;">{{ Session::get('alert') }}</div>
          </div>
          @endif
          @if(\Session::has('alert-success'))
          <div class="alert alert-success">
            <div>{{ Session::get('alert-success') }}</div>
          </div>
          @endif
          {{csrf_field()}}
          <div class="text-center">
             <img class="mb-4 center" src="logo pnj.png" alt="" width="150">
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input name="pass1" type="password" id="inputpass1" class="form-control" placeholder="Masukkan password baru" required>
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input name="pass2" type="password" id="inputpass2" class="form-control" placeholder="Konfirmasi password baru" required>
          </div>
          <div class="form-group">
            <input type="submit" value="Reset" class="btn-sm float-right login_btn" style="font-size: 12px; text-align: center;">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

</body>
</html>