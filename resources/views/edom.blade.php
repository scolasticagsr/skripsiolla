<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>EDOM PNJ | Mahasiswa</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <!-- Custom styles for this template -->
    <link href="edom.css" rel="stylesheet">
  </head>
  <body>
  <header>
    <div class="navbar navbar-expand-md navbar-dark ">
      <a class="navbar-brand" href="#" style="font-weight: bold; vertical-align: middle; font-size: 30px;">Pengisian Evaluasi Dosen oleh Mahasiswa (EDOM)</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto"></ul>
        <form class="form-inline my-2 my-lg-0">
          <a class="btn btn-outline-warning btn-sm float-right" href="/logout" style="text-align: right;" role="button">Logout</a>
        </form>
      </div>
    </div>
  </header>

  <div class="container">
  <br>
  <div class="card text-white">
    <div class="card-header bg-info">
      <main role="main">
      <br>
      <table class="table-borderless" width="800px">
        <tbody>
          <tr>
            <th scope="row" style="font-size: 15.5px; color: black;">Nama Lengkap</th>
            <td style="font-size: 15.5px; color: black;">: {{ Session::get('nama_mahasiswa') }} </td>
          </tr>
            <th scope="row" style="font-size: 15.5px; color: black;">NIM</th>
            <td style="font-size: 15.5px; color: black;">: {{ Session::get('nim') }}</td>
          </tr>
          <tr>
            <th scope="row" style="font-size: 15.5px; color: black;">Jurusan</th>
            <td style="font-size: 15.5px; color: black;">: {{ Session::get('jurusan') }} </td>
          </tr>
            <th scope="row" style="font-size: 15.5px; color: black;">Prodi</th>
            <td style="font-size: 15.5px; color: black;">: {{ Session::get('prodi') }}</td>
          </tr>
        </tbody>
      </table>
      <br>
      </main>
    </div>
    <div class="card-body text-dark">
      <a href="/pdf" class="float-right" style="text-align: right; font-size: 15px;">Print Bukti Pengisian Edom</a>
      <br>
      <h1 style="font-size: 12px; text-align: right;">Jika ada nama dosen yang belum ditampilkan atau tidak sesuai harap menghubungi Ketua atau Sekretaris Jurusan agar melengkapi data Unisyss.</h1>
      <form>
      {{ csrf_field() }}
      <div class="table-responsive">
        <br>
        <table class="table table-bordered table-striped table-sm" id="tableEdom">
          <thead bgcolor='#499e9e'>
            <tr>
              <th style="text-align: center; font-size: 14px;">NO</th>
              <th style="text-align: center; font-size: 14px;">Mata Kuliah</th>
              <th style="text-align: center; font-size: 14px;">Periode</th>
              <th style="text-align: center; font-size: 14px;">Semester</th>
              <th style="text-align: center; font-size: 14px;">Nama Dosen</th>
              <th style="text-align: center; font-size: 14px;">Status</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1; @endphp
            @foreach($data as $jadwal)
            <tr>
              <td style="font-size: 14px; text-align: center;">{{$no++}}</td>
              <td style="font-size: 14px; font-family:">
                  <?php 
                    $con = mysqli_connect("127.0.0.1","edompnjx_root","root1234","edompnjx_skripsiolla");
                    $data = mysqli_query($con, "SELECT mata_kuliah FROM mata_kuliah WHERE kode_matakuliah LIKE '$jadwal->kode_matakuliah'") or die (mysqli_error($con));
                    while ($datas = mysqli_fetch_array($data)) 
                    {
                      echo $datas['mata_kuliah'];
                    }
                  ?>
              </td>
              <td style="font-size: 13px;">{{$jadwal->periode}}</td>
              <td style="font-size: 13px;">{{$jadwal->semester}}</td>
              <td style="font-size: 13px;">
                <?php 
                  $con = mysqli_connect("127.0.0.1","edompnjx_root","root1234","edompnjx_skripsiolla");
                  $data = mysqli_query($con, "SELECT nama_dosen FROM dosen WHERE nip LIKE $jadwal->nip") or die (mysqli_error($con));
                  while ($datas = mysqli_fetch_array($data)) 
                  { 
                    echo $datas['nama_dosen']; 
                  }
                ?>
              </td>
              <td style="font-size: 13px; text-align: center;">
                @if ($jadwal->status == 0)
                <a href="{{route('formedom.edit', $jadwal->id)}}" id="proses">Proses</a>
                @else
                  {{$jadwal->status}}
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </form> 
    </div>
  </div>
  </div>
  </body>
</html>
