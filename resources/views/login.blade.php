<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
  <title>EDOM | Login</title>
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="home.css">
</head>

<body>
<br>
<div class="container">
  <div class="d-flex justify-content-center h-100">
    <h1 style="text-align: center; font-weight: bold; font-size: 29px;"><img class="mb-4" src="logo pnj.png" alt="" width="150" height="100">EVALUASI DOSEN OLEH MAHASISWA</h1>
    <div class="card">
      <div class="card-header">
        <h3>Panel Mahasiswa</h3>
      </div>
      <div class="card-body">
        <form class="form-signin" method="POST" action="/postLogin">
          @if(\Session::has('alert'))
          <div class="alert alert-danger alert-dismissible fade show">
            <div>{{ Session::get('alert') }}</div>
          </div>
          @endif
          @if(\Session::has('alert-success'))
          <div class="alert alert-success">
            <div>{{ Session::get('alert-success') }}</div>
          </div>
          @endif
          {{ csrf_field() }}
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input name="nim" type="nim" id="inputnim" class="form-control" placeholder="NIM" required autofocus>
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input name="password" type="password" id="password" class="form-control" placeholder="Password" required>
          </div>
          <div class="form-group">
            <input type="submit" value="Login" class="btn-sm float-right login_btn" style="font-size: 12px;">
            <a href="/lupapassword" style="color: white; font-size: 13px;">Lupa Password?</a>
          </div>
          </form>
        </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript">
          $('#password').blur(function(){
            var password=$(this).val();
            var len=password.length;
            if (len>0 && len<6) {
                $(this).parent().find('.text-warning').text("");
                $(this).parent().find('.text-warning').text("password minimal 6 karakter");
                return apply_feedback_error(this);
            }
            }
        );
</script> -->
</body>
</html>