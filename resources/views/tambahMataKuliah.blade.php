<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Admin | Mahasiswa</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/dashboard/">

    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.4/examples/dashboard/dashboard.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/admin/home">Admin EDOM PNJ</a>
  <!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> -->
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="/logout">Sign out</a>
    </li>
  </ul>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="{{route('mahasiswa.index')}}">
              <span data-feather="home"></span>
              Mahasiswa <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('jurusan.index')}}">
              <span data-feather="file"></span>
              Jurusan
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('kelas.index')}}">
              <span data-feather="shopping-cart"></span>
              Kelas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="{{route('matakuliah.index')}}">
              <span data-feather="shopping-cart"></span>
              Mata Kuliah
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('dosen.index')}}">
              <span data-feather="users"></span>
              Dosen
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('jadwal.index')}}">
              <span data-feather="bar-chart-2"></span>
              Jadwal
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Tambah Mata Kuliah</h1>
      </div>

<div class="container">
  <form method="POST" action="{{route('matakuliah.store')}}">
      @if(\Session::has('alert'))
      <div class="alert alert-danger">
        <div>{{ Session::get('alert') }}</div>
      </div>
      @endif
      @if(\Session::has('alert-success'))
      <div class="alert alert-success">
        <div>{{ Session::get('alert-success') }}</div>
      </div>
      @endif

  {{ csrf_field() }}
  <div class="form-group">
    <label for="inputkodematkul" class="col-sm-2 col-form-label">Kode Mata Kuliah</label>
    <div class="col-sm-10">
      <input name="kode_matakuliah" type="kode_matakuliah" class="form-control" id="inputkodematkul" value="{{ old('kode_matakuliah') }}" required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputmatakuliah" class="col-sm-2 col-form-label">Mata Kuliah</label>
    <div class="col-sm-10">
      <input name="mata_kuliah" type="mata_kuliah" class="form-control" id="inputmatakuliah" value="{{ old('mata_kuliah') }}" required>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-10" style="text-align: right;">
      <button type="submit" class="btn btn-primary btn-lg btn-sm">Tambah</button>
    </div>
  </div>

  </form>
  </div>
      <!-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> -->

      <!-- <h2>Section title</h2> -->

<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body> -->
</html>
