<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<p align="center" style="font-weight: bold; font-size: 18px;">Bukti Pengisian Formulir</p>
<p align="center" style="font-weight: bold; font-size: 18px;">Evaluasi Dosen Oleh Mahasiswa (EDOM)</p>
<p align="center" style="font-weight: bold; font-size: 18px;">Technic Information Technology / {{ Session::get('prodi') }}
</p>
<p align="center" style="font-weight: bold; font-size: 18px;">Politeknik Negeri Jakarta</p>
<br>
<table class="table table-bordered" style="border-color: black;">
  <thead>
    <tr>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">NO</th>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">Mata Kuliah</th>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">Periode</th>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">Semester</th>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">Nama Dosen</th>
      <th style="text-align: center; font-weight: bold; font-size: 13px;">Status</th>
    </tr>
  </thead>
  <tbody>
    @php $no=1; @endphp
    @foreach($data as $jadwal)
      <tr>
        <td style="text-align: center; font-size: 13px; vertical-align: middle;">{{$no++}}</td>
        <td style="font-size: 13px; vertical-align: middle;">
          <?php 
            $con = mysqli_connect("127.0.0.1","edompnjx_root","root1234","edompnjx_skripsiolla");
            $data = mysqli_query($con, "SELECT mata_kuliah FROM mata_kuliah WHERE kode_matakuliah LIKE '$jadwal->kode_matakuliah'") or die (mysqli_error($con));
            while ($datas = mysqli_fetch_array($data)) 
            {
              echo $datas['mata_kuliah'];
            }
          ?>
        </td>
        <td style="font-size: 13px; vertical-align: middle;">{{$jadwal->periode}}</td>
        <td style="font-size: 13px; vertical-align: middle;">{{$jadwal->semester}}</td>
        <td style="font-size: 13px; vertical-align: middle;">
          <?php 
            $con = mysqli_connect("127.0.0.1","edompnjx_root","root1234","edompnjx_skripsiolla");
            $data = mysqli_query($con, "SELECT nama_dosen FROM dosen WHERE nip LIKE $jadwal->nip") or die (mysqli_error($con));
            while ($datas = mysqli_fetch_array($data)) 
            { 
              echo $datas['nama_dosen']; 
            }
          ?>
        </td>
        <td style="text-align: center; font-size: 13px; vertical-align: middle;">
          @if ($jadwal->status == 0)
          <p>0%</p>
          @else
          {{$jadwal->status}}
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

<p style="font-size: 13px;">Dokumen ini rahasia. Dosen yang bersangkutan tidak mengetahui Mahasiswa yang menilai.</p>
<br>
<br>
<br>
<p align="right">Yang Mengisi EDOM,</p>
<br>
<br>
<br>
<p align="right" class="text-uppercase" style="font-size: 14px;">{{ Session::get('nama_mahasiswa') }} ({{ Session::get('nim')}})</p>
</body>
</html>





