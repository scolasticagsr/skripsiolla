<!doctype html>
<html lang="en" oncontextmenu="return false">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>EDOM PNJ | Mahasiswa</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
    html,body{
      background-color: rgba(83, 181, 181, 0.35);
    }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      .container {
        background-color: "#53B5B5";
      }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="formEdom.css" rel="stylesheet">
  </head>

  <body>
    <header>
    <div class="navbar navbar-expand-md navbar-dark ">
      <div class="container d-flex justify-content-between">
      </div>
    </div>
  </header>

  <main>
  <br>
  <div class="container">
  <div class="card text-white">
    <div class="card-header bg-info">
      <a class="navbar-brand" href="#" style="font-weight: bold; vertical-align: middle; font-size: 30px; color: black;">Pengisian Evaluasi Dosen oleh Mahasiswa (EDOM)</a>
      <p style="font-size: 20px; color: black;">Politeknik Negeri Jakarta</p>
    </div>
    <div class="card-body text-dark">
      <p style="font-size: 15px;">Technic Information Technology / {{ Session::get('prodi') }}</p>
      <div class="table-responsive"> 
      <table class="table table-bordered table-sm"> 
      <thead>
        <tr bgcolor="#53B5B5">
          <th style="font-size: 14px; text-align: center;">Nama Dosen</th>
          <th style="font-size: 14px; text-align: center;">Mata Kuliah</th>
          <th style="font-size: 14px; text-align: center;">Th. Akademik</th>
          <th style="font-size: 14px; text-align: center;">Semester</th>  
        </tr>
      </thead>
      <tbody> 
        <tr>
          <td style="font-size: 14px;">{{ Session::get('nama_dosen') }}</td>
          <td style="font-size: 14px;">{{ Session::get('mata_kuliah') }}</td>
          <td style="font-size: 14px;">{{ Session::get('periode') }}</td>
          <td style="font-size: 14px;">{{ Session::get('semester') }}</td>
        </tr>
      </tbody>
    </table>
      </div>
      <table class="table-borderless" align="right">
        <tbody>
          <p style="font-size: 14px; font-weight: bold;">Keterangan :</p>
          <p style="font-size: 14px; font-weight: bold;">Skala Penilaian: 1 = Sangat Kurang; 2 = Agak Kurang; 3 = Kurang; 4 = Baik; 5 = Agak Baik; 6 = Sangat Baik</p>
        </tbody>
      </table>
    @foreach ($data2 as $data2s)
    <form method="POST" action="{{route('formedom.update', $data2s->id)}}" id="edom" onsubmit="return confirm('Sudah yakin dengan isian Anda?');">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
      <div class="table-responsive form-group">
        <table class="table table-bordered table-striped table-sm" style="border-color: #53B5B5;" >
          <thead>
            <tr bgcolor="#53B5B5">
              <th scope="col" colspan=8 style="text-align: left; font-weight: bold; font-size: 18px;">Komponen Penilaian</th>
            </tr>
            <tr>
              <th style="text-align: center;">No.</th>
              <th style="text-align: center;">Aspek yang dinilai</th>
              <th style="text-align: center;">1</th>
              <th style="text-align: center;">2</th>
              <th style="text-align: center;">3</th>
              <th style="text-align: center;">4</th>
              <th style="text-align: center;">5</th>
              <th style="text-align: center;">6</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1; @endphp
            @foreach($data as $datas)
            <tr>
              <td>{{ $no++ }}</td>
              <td style="font-size: 13.5px; vertical-align: middle;">{{$datas->pertanyaan}}</td>
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">  
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai1" value="1" required/>
                  </div>
              </td>  
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai2" value="2"/>
                  </div>
              </td>   
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai3" value="3"/>
                  </div>
              </td>
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai4" value="4"/>
                  </div>
              </td>
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai5" value="5"/>
                  </div> 
              </td>
              <td>
                  <div class="custom-control custom-radio" style="text-align: center;">
                    <input class="form-check-input positionn-static" type="radio" name="nilai[<?php echo $datas['id'];?>]" id="nilai6" value="6"/>
                  </div> 
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="form-group" align="right">
        <div class="col-sm-10" style="text-align: right;">
          <button type="submit" name="submit" class="btn btn-success btn-sm">Submit</button>
        </div>
      </div>
    </form>
    @endforeach
  </div>
    </div>
  </div>
    
  </main>
  </body>
</html>


