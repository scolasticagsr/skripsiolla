
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>EDOM | Politeknik Negeri Jakarta</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">

    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="home2.css" rel="stylesheet">
  </head>
  <body>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <a class="btn btn-warning btn-sm float-right" href="/login" role="button">Login</a>
    </div>
  </nav>

  <main role="main">
    <div class="jumbotron">
      <div class="container"></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2 style="font-size: 25px;">Apa itu EDOM?</h2>
          <p style="font-size: 15px; text-align: justify;">Evaluasi Dosen oleh Mahasiswa atau EDOM merupakan sebuah media penilaian atas kinerja dosen dalam proses pembelajaran selama satu semester yang dilakukan oleh seluruh mahasiswa PNJ. Pengisian EDOM ini akan dilaksanakan di setiap akhir semester dan bukti pengisian EDOM pun akan menjadi salah satu syarat pengambilan marskheet. EDOM ini bersifat anonim atau rahasia.</p>
        </div>
        <div class="col-md-4">
          <h2 style="font-size: 25px;">Bagaimana caranya?</h2>
          <p style="font-size: 15px; text-align: justify;">Mahasiswa melakukan login ke dalam sistem EDOM menggunakan NIM dan password yang telah diberikan pada awal menjadi mahasiswa baru Politeknik Negeri Jakarta melalui email mahasiswa. Jika informasi login sesuai, maka sistem akan mengirimkan kode OTP ke email Anda dan lanjut ke verifikasi OTP. Bila berhasil, mahasiswa mengisi EDOM untuk setip-setiap dosen.</p>
        </div>
        <div class="col-md-4">
          <h2 style="font-size: 25px;">Apakah wajib mengisi EDOM?</h2>
          <p style="font-size: 15px; text-align: justify;">Ya, seluruh mahasiswa PNJ wajib mengisi EDOM di setiap akhir semester. Selain sebagai evaluasi kinerja dosen, EDOM juga berguna sebagai salah satu bukti persyaratan pengambilan marksheet.</p>
        </div>
      </div>
      <hr>
    </div>
  </main>
  </body>
</html>
